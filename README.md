# Frictionless Platform based on Spherical Air Bearing

This is the design of a Frictionless Platform based on Spherical air bearing that we made for the Final Degree Project to obtain the title of Mechatronics Engineering. The title of the Thesis is: "Design and Implementation of a Frictionless Platform for Testing Attitude Determination and Control System of Nanosatellites".  

It contains two differents designs of a Spherical Air Bearing and the design of a Platform with a Mass Balancing System. 

Design1_Spherical_Air_Bearing.zip contains the active and the pasive part of the Spherical Air Bearing of the first design with a ratio of 75 mm, and contains two more components that I use to simulate the flow of the air inside the air bearing.

Design2_Spherical_Air_Bearing.zip contains the active and the pasive part of the Spherical Air Bearing of the second design with a ratio of 40 mm, and contains two more components that I use to simulate the flow of the air inside the air bearing. It also has the design of a cover to put on the cup of the air bearing.

Platform_with_Mass_Balancing_System.zip contains the design of a platform for the spherical air bearing (for both designs), with a Mass Balancing System. The design of all components are in the folder.